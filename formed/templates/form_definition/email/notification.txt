{% load definition_form_tags %}
== {{ form_definition.name }} ==

{% for page in pages %}{% if page.name %}
=== {{ page.name }} ===
{% endif %} {% for fieldset in page.fieldsets %} {% if fieldset.legend %}
==== {{ fieldset.legend }} ====
{% endif %}{% for row in fieldset.rows %}{% for field in row.fields %}{% field_value field.name form_submission.submission as value %}
{{ field.label }}: {% spaceless %}
{% if field.is_boolean_valued %}
{{ value|yesno }}
{% elif field.is_multi_valued %}
{# Translators: Default display value for an empty list #}
{{ value|join:', '|default:_('&lt;none&gt;') }}
{% else %}
{# Translators: Default display value for an empty value #}
{{ value|default:_('&lt;empty&gt;') }}
{% endif %}
{% endspaceless %}
{% endfor %}{% endfor %}{% endfor %}{% endfor %}
